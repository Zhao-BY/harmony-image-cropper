package com.theartofdev.edmodo.cropper;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

/**
 * @Author Zhao Baiyi
 * <p>
 * 此类是一个工具类
 * <p>
 * 此类是面向于用户的 面向于MainAbilitySlice的
 * <p>
 * 此类提供设置一些参数的功能，提供了页面跳转，跳转到CropImageActivity的方法，有一些属性暂时用不到。
 */

public final class CropImage {

    //存一个context
    private static Context mContext;

    //图片资源文件
    private static int msource;

    //裁减结果
    public static final boolean CROPFLAG = true;

    //空的构造器
    private CropImage() {
    }

    //内部类初始化
    public static ActivityBuilder activity() {
        return new ActivityBuilder(null , null , null , 0);
    }

    /**
     * 这个方法对传进来的 component进行操作
     * 给这个空的component增加图片 旋转等等
     * @param result 一个参数用来判断返回的结果 以及如何处理
     * @param component 对这个component进行操作
     */
    public static void handleImage(int result, Component component) {

        //203表示裁剪成功（203为自定义的数字）
        if (result == 203) {
            //获得原始位图
            PixelMap pixelMap = PixelMapUtils.getOriginalPixelMap(mContext , msource).get();

            //创建屏幕工具类 获得屏幕的宽度
            CropWindowHandler windowHandler = new CropWindowHandler(mContext);

            /**
             * 缩放指数 原始的图片的缩放大小
             * 数值为原始图片的宽度（pixelMap.getImageInfo().size.width）除以屏幕的宽度（windowHandler.getWindowWidth()） 的倒数
             */
            float ratio = (float) windowHandler.getWindowWidth()/(float) pixelMap.getImageInfo().size.width;

            //获得裁剪后的位图
            PixelMap cropped = CropOverlayView.getCroppedPixelMap();
            PixelMapHolder pixelMapHolder = new PixelMapHolder(cropped);

            //创建bitmaputils工具类，获得位图相关数据
            PixelMapUtils pixelMapUtils = new PixelMapUtils(mContext, cropped, 400, msource);

            /**
             * 创建画位图的方法
             * 获取到之前的点击动作数组 这样就可以获得到具体最终的图片的方向
             * 并且依次按照图片方向进行旋转
             */
            Component.DrawTask drawTask = new Component.DrawTask() {
                @Override
                public void onDraw(Component component, Canvas canvas) {

                    //获得行为动作
                    int[] action = CropImageView.getActionResult();
                    //获得动作数目
                    int actionIndex = CropImageView.getActionIndexResult();
                    //循环执行旋转、翻转动作
                    for (int i = 0; i < actionIndex; i++) {
                        if (action[i] == 1) {
                            //rotate图像
                            canvas.rotate(90, pixelMapUtils.getRealPixelMapWidth() / 2 * ratio, pixelMapUtils.getRealPixelMapHeight() / 2 * ratio);
                        } else if (action[i] == 2) {
                            //水平翻转
                            //向下移动高度
                            canvas.translate(pixelMapUtils.getRealPixelMapWidth() * ratio, 0);
                            //向y轴负方向缩放一倍
                            canvas.scale(-1f, 1f);
                        } else if (action[i] == 3) {
                            //垂直翻转
                            //向下移动高度
                            canvas.translate(0, pixelMapUtils.getRealPixelMapHeight() * ratio);
                            //向y轴负方向缩放一倍
                            canvas.scale(1f, -1f);
                        }
                    }

                    //按照原来的比例进行缩放
                    canvas.scale(ratio , ratio);
                    //画图
                    canvas.drawPixelMapHolder(pixelMapHolder, 0, 0, new Paint());
                }
            };

            //为component增加drawtask方法
            component.addDrawTask(drawTask);
        }
    }

    public static PixelMap getCroppedPixelMap(){
        return CropOverlayView.getCroppedPixelMap();
    }

    public static final class ActivityBuilder {

        //设置图片的Uri
        private final Uri mSource;

        //需要跳转回的bundle的名字
        private String bundleName;

        //需要跳转回的Ability的名字
        private String abilityName;

        //request_code
        private int request_code;

        //初始化 记录一些信息
        private ActivityBuilder(Uri source , String bundle , String ability , int request) {
            mSource = source;
            bundleName = bundle;
            abilityName = ability;
            request_code = request;
        }

        //返回一个intent
        public Intent getIntent(Context context) {
            return getIntent(context, CropImageAbility.class);
        }

        //返回一个intent 用来实现页面跳转
        public Intent getIntent(Context context, Class<?> cls) {
            Intent intent = new Intent();
            intent.setParam("source", msource);
            if(bundleName != null){
                intent.setParam("bundleName" , bundleName);
            }
            if(abilityName != null){
                intent.setParam("abilityName" , abilityName);
            }
            if(request_code != 0){
                intent.setParam("request_code" , request_code);
            }
            return intent;
        }

        //页面跳转
        public void start(Ability ability, AbilitySlice abilitySlice) {
            start(ability, abilitySlice, request_code);
        }

        //页面跳转
        public void start(Ability ability, AbilitySlice abilitySlice, int requestCode) {
            //给crop添加操作
            AbilitySlice cropImageAbilitySlice = new CropImageAbility();
            abilitySlice.presentForResult(cropImageAbilitySlice, getIntent(ability), requestCode);
        }

        //设置资源图片，被裁减的图片的id
        public ActivityBuilder setSource(int source) {
            msource = source;
            return this;
        }

        //设置context
        public ActivityBuilder setContext(Context context) {
            mContext = context;
            return this;
        }

        //设置需要跳转回的bundle名字
        public ActivityBuilder setBundleName(String s){
            bundleName = s;
            return this;
        }

        //设置需要跳转回的ability名字
        public ActivityBuilder setAbilityName(String s){
            abilityName = s;
            return this;
        }

        //设置需要跳转回时的code
        public ActivityBuilder setRequset_code(int i){
            request_code = i;
            return this;
        }

    }


}
