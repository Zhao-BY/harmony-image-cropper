package com.theartofdev.edmodo.cropper;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.RectFloat;
import ohos.bundle.AbilityInfo;
import ohos.media.image.PixelMap;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_PARENT;

/**
 * @Author Zhao Baiyi
 *
 * 此类为裁剪功能实现的主要页面
 *
 * 此页面上显示了一些功能性的button
 * 还有被裁减图片以及裁剪框
 */

public class CropImageAbility extends AbilitySlice {

    //定义此slice的Dependent布局
    private DependentLayout myLayout = new DependentLayout(this);

    //被裁减图片
    private Component mPicture;

    //被裁减图片的位图
    private PixelMap mPixelMap;

    //位图工具类
    private PixelMapUtils mPixelMapUtils;

    //屏幕工具类
    private CropWindowHandler mCropWindowHandler;

    //裁剪框
    private Component mCropBound;

    //裁剪框工具类
    private CropOverlayView mCropOverlayView;

    //图片资源
    private int mSource;

    //图片上边距
    private final int topIndex = 400;

    //图片工具类
    private CropImageView mCropImageView;

    //裁减结果的矩阵
    public static RectFloat croppedRectFloat;

    //裁减结果的pixelmap
    public static PixelMap croppedPixelMap;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        //创建本slice的布局文件
        DependentLayout.LayoutConfig config = new DependentLayout.LayoutConfig(MATCH_PARENT, MATCH_PARENT);

        //设置默认竖屏
        setDisplayOrientation(AbilityInfo.DisplayOrientation.PORTRAIT);
        //设置布局的背景
        myLayout.setLayoutConfig(config);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(new RgbColor(255, 255, 255));
        myLayout.setBackground(element);

        //设置button,image等
        setupViews(intent);

        //加载裁剪框、背景图片等等
        loadInput();

        //加载布局
        super.setUIContent(myLayout);
    }

    //按钮和图片初始化
    private void setupViews(Intent intent) {
        buttonInit(intent);
        imageInit(intent);
    }

    //按钮初始化
    private void buttonInit(Intent intent) {
        //创建button
        Button cancel = cancelButton(intent);
        Button rotate = rotateButton();
        Button horfilp = horizontalFilpButton();
        Button verfilp =  verticalFilpButton();

        Button crop = cropButton(intent);

        //将button添加到布局
        myLayout.addComponent(cancel);
        myLayout.addComponent(rotate);
        myLayout.addComponent(horfilp);
        myLayout.addComponent(verfilp);
        myLayout.addComponent(crop);
    }

    //取消按钮
    private Button cancelButton(Intent intent) {
        //创建取消button
        Button cancel = new Button(this);
        //为button增加布局条件
        DependentLayout.LayoutConfig cancelLayoutConfig = new DependentLayout.LayoutConfig();
        cancelLayoutConfig.setMargins(40, 50, 0, 0);
        cancelLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_TOP);
        cancelLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_LEFT);
        //设置背景颜色
        cancel.setLayoutConfig(cancelLayoutConfig);
        ShapeElement cancelElement = new ShapeElement();
        cancelElement.setRgbColor(new RgbColor(155, 155, 155));
        cancelElement.setCornerRadius(25);
        cancel.setBackground(cancelElement);
        //设置文本
        cancel.setText("cancel");
        cancel.setTextSize(40);
        cancel.setHeight(180);
        cancel.setWidth(220);
        //绑定点击方法
        cancel.setClickedListener(new Component.ClickedListener() {
            public void onClick(Component v) {
                //为按钮绑定方法
                cancel(intent);
            }
        });

        return cancel;
    }

    //旋转按钮
    private Button rotateButton() {
        //创建旋转button
        Button rotate = new Button(this);
        //为button增加布局条件
        DependentLayout.LayoutConfig rotateLayoutConfig = new DependentLayout.LayoutConfig();
        rotateLayoutConfig.setMargins(300, 50, 0, 0);
        rotateLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_TOP);
        rotateLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_LEFT);
        rotate.setLayoutConfig(rotateLayoutConfig);
        //设置背景颜色
        ShapeElement rotateElement = new ShapeElement();
        rotateElement.setRgbColor(new RgbColor(255, 228, 181));
        rotateElement.setCornerRadius(25);
        rotate.setBackground(rotateElement);
        //设置文本
        rotate.setText("rotate");
        rotate.setTextSize(40);
        rotate.setHeight(180);
        rotate.setWidth(220);
        //绑定点击方法
        rotate.setClickedListener(new Component.ClickedListener() {
            public void onClick(Component v) {
                rotate();
            }
        });

        return rotate;
    }

    //水平翻转按钮
    private Button horizontalFilpButton() {
        //创建翻转button
        Button filp = new Button(this);
        //为button增加布局条件
        DependentLayout.LayoutConfig filpLayoutConfig = new DependentLayout.LayoutConfig();
        filpLayoutConfig.setMargins(0, 50, 300, 0);
        filpLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_TOP);
        filpLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_RIGHT);
        //设置背景颜色
        filp.setLayoutConfig(filpLayoutConfig);
        ShapeElement filpElement = new ShapeElement();
        filpElement.setRgbColor(new RgbColor(180, 238, 180));
        filpElement.setCornerRadius(25);
        filp.setBackground(filpElement);
        //设置文本
        filp.setText("horFilp");
        filp.setTextSize(40);
        filp.setHeight(85);
        filp.setWidth(220);
        //绑定点击方法
        filp.setClickedListener(new Component.ClickedListener() {
            public void onClick(Component v) {
                horizontalFlip();
            }
        });

        return filp;
    }

    //垂直翻转按钮
    private Button verticalFilpButton() {
        //创建翻转button
        Button filp = new Button(this);
        //为button增加布局条件
        DependentLayout.LayoutConfig filpLayoutConfig = new DependentLayout.LayoutConfig();
        filpLayoutConfig.setMargins(0, 145, 300, 0);
        filpLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_TOP);
        filpLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_RIGHT);
        //设置背景颜色
        filp.setLayoutConfig(filpLayoutConfig);
        ShapeElement filpElement = new ShapeElement();
        filpElement.setRgbColor(new RgbColor(180, 238, 180));
        filpElement.setCornerRadius(25);
        filp.setBackground(filpElement);
        //设置文本
        filp.setText("verFilp");
        filp.setTextSize(40);
        filp.setHeight(85);
        filp.setWidth(220);
        //绑定点击方法
        filp.setClickedListener(new Component.ClickedListener() {
            public void onClick(Component v) {
                verticalFlip();
            }
        });

        return filp;
    }

    //裁剪按钮
    private Button cropButton(Intent intent) {
        //创建裁剪button
        Button crop = new Button(this);
        //为button增加布局条件
        DependentLayout.LayoutConfig cropLayoutConfig = new DependentLayout.LayoutConfig();
        cropLayoutConfig.setMargins(0, 50, 40, 0);
        cropLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_TOP);
        cropLayoutConfig.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_RIGHT);
        //设置背景颜色
        crop.setLayoutConfig(cropLayoutConfig);
        ShapeElement cropElement = new ShapeElement();
        cropElement.setRgbColor(new RgbColor(0, 125, 155));
        cropElement.setCornerRadius(25);
        crop.setBackground(cropElement);
        //设置文本
        crop.setText("crop");
        crop.setTextSize(40);
        crop.setHeight(180);
        crop.setWidth(220);
        //绑定点击方法
        crop.setClickedListener(new Component.ClickedListener() {
            public void onClick(Component v) {
                crop(intent);
            }
        });

        return crop;
    }

    //图片初始化
    private void imageInit(Intent intent) {

        //获得图片的id
        int source = intent.getIntParam("source", 0);
        mSource = source;

        //根据图片id获取pixelmap
        PixelMap pixelMapOriginal = PixelMapUtils.getOriginalPixelMap(this, mSource).get();
        mPixelMap = pixelMapOriginal;

        //创建bitmaputils工具类，获得位图相关数据
        PixelMapUtils pixelMapUtils = new PixelMapUtils(this , mPixelMap ,topIndex , mSource );

        //创建cropwindowhandler工具类，获得windows相关数据
        CropWindowHandler cropWindowHandler = new CropWindowHandler(this);

        //创建图片工具类，用来获得图片
        mCropImageView = new CropImageView(mPixelMap , this ,  mSource , topIndex);

        //获取展示图片的component
        mPicture = mCropImageView.getmPicture();

        //计算图片的位置，使得图片居中显示，计算出图片距离屏幕左边的空白
        int margin = cropWindowHandler.getWindowWidth()/2 - pixelMapUtils.getPixelMapWidth()/2;

        //给mPicture增加布局
        DependentLayout.LayoutConfig componentLayoutConfig = new DependentLayout.LayoutConfig();
        componentLayoutConfig.setMargins(0, topIndex, 0, 0);//边距
        mPicture.setLayoutConfig(componentLayoutConfig);

        //将图片加入布局
        myLayout.addComponent(mPicture);

    }

    //初始化工具类，加载裁剪框等等
    private void loadInput() {

        //创建位图工具类
        mPixelMapUtils = new PixelMapUtils(this, mPixelMap, 400, mSource);

        //创建屏幕工具类
        mCropWindowHandler = new CropWindowHandler(this);

        //创建裁剪框的工具类
        mCropOverlayView = new CropOverlayView(this, mPixelMapUtils, mCropWindowHandler);

        //获得裁剪框
        mCropBound = mCropOverlayView.getmCropBound();

        //将裁剪框加入布局文件
        myLayout.addComponent(mCropBound);
    }

    //取消裁剪方法
    private void cancel(Intent intentOriginal) {
        Intent intent = new Intent();

        //增加裁剪状态及结果
        intent.setParam("cropFlag", !CropImage.CROPFLAG);
        intent.setParam("cropStatus", 1);

        // 通过Intent中的OperationBuilder类构造operation对象，指定设备标识（空串表示当前设备）、应用包名、Ability名称
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(intentOriginal.getStringParam("bundleName"))
                .withAbilityName(intentOriginal.getStringParam("abilityName"))
                .build();
        // 把operation设置到intent中
        intent.setOperation(operation);

        //跳转
        startAbility(intent);
    }

    //图片旋转的方法
    private void rotate(){
        //图片旋转
        mCropImageView.rotateOnce();
        //裁剪框旋转
        mCropOverlayView.rotateOnce();
    }

    //图片水平翻转的方法
    private void horizontalFlip(){
        //图片翻转
        mCropImageView.horizontalFilp();
        //裁剪框翻转
        mCropOverlayView.horizontalFilpOnce();
    }

    //图片垂直翻转的方法
    private void verticalFlip(){
        mCropImageView.verticalFilp();
        //裁剪框翻转
        mCropOverlayView.verticalFilpOnce();
    }

    //成功裁剪方法
    private void crop(Intent intentOriginal) {

        //计算裁减后的pixelmap并存放于cropoverlayview中
        mCropOverlayView.croppedPixel(this);

        //显示到MainActivity
        Intent intent = new Intent();

        //增加裁剪状态及结果
        intent.setParam("cropFlag", CropImage.CROPFLAG);
        intent.setParam("cropStatus", 2);
        intent.setParam("result_code" , 203);

        RectFloat cropRect = mCropOverlayView.getmCropRect();
        //塞入裁剪结果
        intent.setParam("resultImg", mSource);
        croppedRectFloat = mCropOverlayView.getmCropRect();
        croppedPixelMap = mPixelMap;

        // 通过Intent中的OperationBuilder类构造operation对象，指定设备标识（空串表示当前设备）、应用包名、Ability名称
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(intentOriginal.getStringParam("bundleName"))
                .withAbilityName(intentOriginal.getStringParam("abilityName"))
                .build();
        // 把operation设置到intent中
        intent.setOperation(operation);

        //跳转
        startAbility(intent);

    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
