package com.theartofdev.edmodo.cropper;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * @Author Zhao Baiyi
 *
 * 此类为一个工具类
 *
 * 提供了查询windows，也就是屏幕宽和高的方法
 */

final class CropWindowHandler {

    //记录一个上下文环境
    private Context mContext;

    //构造器
    public CropWindowHandler(Context context){
        mContext = context;
    }

    //返回屏幕宽度
    public int getWindowWidth(){
        //获取手机屏幕大小
        DisplayManager displayManager = DisplayManager.getInstance();
        Display display = displayManager.getDefaultDisplay(mContext).get();
        DisplayAttributes displayAttributes = display.getAttributes();

        return displayAttributes.width;
    }

    //返回屏幕高度
    public int getWindowHeight(){
        //获取手机屏幕大小
        DisplayManager displayManager = DisplayManager.getInstance();
        Display display = displayManager.getDefaultDisplay(mContext).get();
        DisplayAttributes displayAttributes = display.getAttributes();

        return displayAttributes.height;
    }
}
