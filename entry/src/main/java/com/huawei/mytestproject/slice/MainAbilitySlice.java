package com.huawei.mytestproject.slice;

import com.huawei.mytestproject.ResourceTable;
import com.theartofdev.edmodo.cropper.CropImage;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.media.image.PixelMap;

/*
 *
 * @Author ZhaoBY
 */

public class MainAbilitySlice extends AbilitySlice {

    //定义一个图片
    Component image;
    //定义一个文本
    Text text;

    @Override
    public void onStart(Intent intent) {
        //重写onstart方法并加载布局文件
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_MainAbility_layout);

        //获取图片对象对应的component
        image = findComponentById(ResourceTable.Id_result_image);

        /*
         * 如果接收的cropFlag为true
         * 处理剪裁后的图片
         * 否则跳过
         */
        if(intent.getBooleanParam("cropFlag",false)){
            handleCrop(intent);
        }

        /* 自定义--获取文本对象对应的component
         * 根据intent里面的cropStatus来显示不同的文本
         * 0表示未接收到数据
         * 1表示剪裁取消
         * 2表示剪裁成功 有数据
         */
        text = (Text) findComponentById(ResourceTable.Id_text);
        if(intent.getIntParam("cropStatus",0) == 0){
            text.setText("欢迎使用");
        }else if(intent.getIntParam("cropStatus",0) == 1){
            text.setText("剪裁取消");
        }else if(intent.getIntParam("cropStatus",0) == 2){
            text.setText("剪裁成功");
        }

        //获取button对象对应的component
        Button button = (Button) findComponentById(ResourceTable.Id_button);
        // 设置button的属性及背景
        ShapeElement background = new ShapeElement();
        background.setRgbColor(new RgbColor(0, 125, 255));
        background.setCornerRadius(25);
        button.setBackground(background);
        if (button != null) {
            // 绑定点击事件
            button.setClickedListener(new Component.ClickedListener() {
                public void onClick(Component v) {
                    begincrop();
                }
            });
        }
    }

    public void begincrop(){
        CropImage.activity()
                .setContext(this)
                .setSource(ResourceTable.Media_cat_square)
                .setBundleName("com.huawei.mytestproject")
                .setAbilityName("com.huawei.mytestproject.MainAbility")
                .setRequset_code(1234)
                .start(super.getAbility(),this);
    }

    //处理剪裁结果
    private void handleCrop(Intent result) {
        int resultImg = result.getIntParam("resultImg",0);
        int result_code = result.getIntParam("result_code" , 0);
        if(resultImg != 0){
            //传入一个component 交给此函数处理
            CropImage.handleImage(result_code , image);
            //获取到裁剪后的位图
            //PixelMap croppedPixelMap = CropImage.getCroppedPixelMap();
        }
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


}
